# Go Options
GO      := go
MODULE  := gitlab.com/argonauts.io/argo
TAGS    :=
GOFLAGS :=
LDFLAGS := -w -s
BINDIR  := $(CURDIR)/bin

# Version Info
GIT_COMMIT     ?= $(shell git rev-parse HEAD)
GIT_SHA        ?= $(shell git rev-parse --short HEAD)
GIT_TAG        ?= $(shell git describe --tags --abbrev=0 --exact-match 2>/dev/null)
GIT_TREE_STATE ?= $(shell test -n "`git status --porcelain`" && echo "dirty" || echo "clean")
ifeq ("$(GIT_TAG)","")
	VERSION ?= $(GIT_SHA)
endif
VERSION ?= ${GIT_TAG}

# Dependencies
HAS_GIT := $(shell command -v git;)
HAS_PROTOC := $(shell command -v protoc;)
HAS_PROTOC_GEN_GO := $(shell PATH=./bin:$PATH command -v protoc-gen-go;)

.PHONY: info
info:
	@echo "Version:        ${VERSION}"
	@echo "Git Tag:        ${GIT_TAG}"
	@echo "Git Commit:     ${GIT_COMMIT}"
	@echo "Git Tree State: ${GIT_DIRTY}"

.PHONY: build
build: protoc
build: LDFLAGS += -X $(MODULE)/pkg/version.Timestamp=$(shell date +%s)
build: LDFLAGS += -X $(MODULE)/pkg/version.GitCommit=${GIT_COMMIT}
build: LDFLAGS += -X $(MODULE)/pkg/version.GitTreeState=${GIT_TREE_STATE}
build: LDFLAGS += -X $(MODULE)/pkg/version.Version=${VERSION}
build:
	GOBIN=$(BINDIR) $(GO) install $(GOFLAGS) -tags '$(TAGS)' -ldflags '$(LDFLAGS)' ./cmd/argo

.PHONY: protoc
protoc:
ifndef HAS_PROTOC
	$(error You must install protoc)
endif
ifndef HAS_PROTOC_GEN_GO
	go build -o $(BINDIR) github.com/golang/protobuf/protoc-gen-go
endif
	$(MAKE) -C api/proto all
