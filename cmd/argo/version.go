package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/argonauts.io/argo/pkg/version"
)

// versionCmd returns a version CLI command which prints version
// information to stdout
func versionCmd() *cobra.Command {
	var j bool
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Print version information",
		Run:   runVersionCmd(&j),
	}
	cmd.Flags().BoolVarP(&j, "json", "j", false, "Print version as JSON")
	return cmd
}

// runVersionCmd
func runVersionCmd(j *bool) func(*cobra.Command, []string) {
	return func(*cobra.Command, []string) {
		if *j {
			b, err := version.JSON()
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Println(string(b))
			}
		} else {
			tsi, err := strconv.ParseInt(version.Timestamp, 10, 64)
			if err == nil {
				tsx := time.Unix(tsi, 0).UTC()
				fmt.Println("Built:", tsx.Format("Monday 2 January at 3:04PM MST"))
			}
			fmt.Println("Version:", version.Version)
			major, minor, patch, meta := version.SemVer()
			fmt.Println("Major:", major)
			fmt.Println("Minor:", minor)
			fmt.Println("Patch:", patch)
			fmt.Println("Meta:", meta)
			fmt.Println("Commit:", version.GitCommit)
			fmt.Println("Tree State:", version.GitTreeState)
		}
	}
}
