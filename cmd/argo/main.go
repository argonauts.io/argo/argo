package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// main is the application entrypoint executing the CLI
func main() {
	if err := mainCmd().Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// mainCmd returns the base CLI command
func mainCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "argo",
		Short: "Run Argo, the helm chart repository.",
	}
	cmd.AddCommand(versionCmd())
	return cmd
}
