package version // import "gitlab.com/argonauts.io/argo/pkg/version"

import (
	"encoding/json"
	"strconv"
	"strings"

	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/argonauts.io/argo/pkg/proto/api/verison"
)

// Version information set a build time
var (
	Version      string
	Timestamp    string
	GitCommit    string
	GitTreeState string
)

// SemVer returns the version broken into its constituent parts
func SemVer() (string, string, string, string) {
	var (
		major string
		minor string
		patch string
		meta  string
	)
	parts := strings.Split(Version, ".")
	switch len(parts) {
	case 1:
		major = parts[0]
	case 2:
		major = parts[0]
		minor = parts[1]
	case 3:
		major = parts[0]
		minor = parts[1]
		patch = parts[2]
	}
	patchMeta := strings.Split(patch, "+")
	if len(patchMeta) == 2 {
		patch = patchMeta[0]
		meta = patchMeta[1]
	}
	return major, minor, patch, meta
}

// JSON returns the version information as JSON
func JSON() ([]byte, error) {
	major, minor, patch, meta := SemVer()
	return json.Marshal(&struct {
		Version      string `json:"version"`
		Major        string `json:"major"`
		Minor        string `json:"minor"`
		Patch        string `json:"patch"`
		Meta         string `json:"meta"`
		Timestamp    string `json:"timestamp"`
		GitCommit    string `json:"gitCommit"`
		GitTreeState string `json:"gitTreeState"`
	}{
		Version,
		major,
		minor,
		patch,
		meta,
		Timestamp,
		GitCommit,
		GitTreeState,
	})
}

// Proto returns version information as a Protocol buffer
func Proto() *version.Version {
	ts, _ := strconv.ParseInt(Timestamp, 10, 64)
	return &version.Version{
		SemVer: Version,
		Timestamp: &timestamp.Timestamp{
			Seconds: ts,
		},
		GitCommit:    GitCommit,
		GitTreeState: GitTreeState,
	}
}
